﻿using AutoFixture;
using Bogus;
using Moq;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Repositories;
using pottencial_payment.Domain.Services;
using pottencial_payment.Domain.Settings;
using pottencial_payment.Test.Configs;
using System.Linq.Expressions;

namespace pottencial_payment.Test.Service
{
    [Trait("Service", "Service Vendedor")]
    public class VendedorServiceTest
    {
        private readonly Mock<IVendedorRepository> _mockVendedorRepository;
        private readonly Mock<AppSetting> _mockAppSetting;
        private readonly Faker _faker;
        private readonly Fixture _fixture;

        public VendedorServiceTest()
        {
            _mockVendedorRepository = new Mock<IVendedorRepository>();
            _faker = new Faker();
            _fixture = FixtureConfig.Get();
            _mockAppSetting = new Mock<AppSetting>();
        }

        [Fact(DisplayName = "Lista Vendedor")]
        public async Task Get()
        {
            var entities = _fixture.Create<IEnumerable<Vendedor>>();

            _mockVendedorRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<Vendedor, bool>>>())).ReturnsAsync(entities);

            var service = new VendedorService(_mockVendedorRepository.Object, _mockAppSetting.Object);

            var response = await service.ObterTodosAsync();

            Assert.NotNull(response);
        }

        [Fact(DisplayName = "Busca Vendedor Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockVendedorRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<Vendedor, bool>>>())).ReturnsAsync(entity);

            var service = new VendedorService(_mockVendedorRepository.Object, _mockAppSetting.Object);

            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Vendedor")]
        public async Task Post()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockVendedorRepository.Setup(mock => mock.AddAsync(It.IsAny<Vendedor>())).Returns(Task.CompletedTask);

            var service = new VendedorService(_mockVendedorRepository.Object, _mockAppSetting.Object);

            try
            {
                await service.AdicionarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Edita Vendedor Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockVendedorRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<Vendedor, bool>>>())).ReturnsAsync(entity);
            _mockVendedorRepository.Setup(mock => mock.EditAsync(It.IsAny<Vendedor>())).Returns(Task.CompletedTask);

            var service = new VendedorService(_mockVendedorRepository.Object, _mockAppSetting.Object);

            try
            {
                await service.AlterarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Remove Vendedor Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockVendedorRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockVendedorRepository.Setup(mock => mock.RemoveAsync(It.IsAny<Vendedor>())).Returns(Task.CompletedTask);
            var service = new VendedorService(_mockVendedorRepository.Object, _mockAppSetting.Object);

            try
            {
                await service.DeletarAsync(entity.Id);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

    }
}