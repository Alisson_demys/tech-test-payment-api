﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Infrastructure.Context;
using pottencial_payment.Infrastructure.Repositories;
using pottencial_payment.Test.Configs;

namespace pottencial_payment.Test.Repository
{
    [Trait("Repository", "Repository Vendedor")]
    public class VendedorRepositoryTest
    {
        private readonly Mock<PottencialContext> _mockContext;
        private readonly Fixture _fixture;

        public VendedorRepositoryTest()
        {
            _mockContext = new Mock<PottencialContext>(new DbContextOptionsBuilder<PottencialContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Lista Vendedores")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Vendedor>>();

            _mockContext.Setup(mock => mock.Set<Vendedor>()).ReturnsDbSet(entities);

            var repository = new VendedorRepository(_mockContext.Object);

            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Vendedor Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockContext.Setup(mock => mock.Set<Vendedor>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new VendedorRepository(_mockContext.Object);

            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Vendedor")]
        public async Task Post()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockContext.Setup(mock => mock.Set<Vendedor>()).ReturnsDbSet(new List<Vendedor> { new Vendedor() });

            var repository = new VendedorRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.AddAsync(entity));
            Assert.Null(exception);

        }

        [Fact(DisplayName = "Edita Vendedor Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockContext.Setup(mock => mock.Set<Vendedor>()).ReturnsDbSet(new List<Vendedor> { new Vendedor() });

            var repository = new VendedorRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.EditAsync(entity));
            Assert.Null(exception);

        }

        [Fact(DisplayName = "Remove Vendedor Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Vendedor>();

            _mockContext.Setup(mock => mock.Set<Vendedor>()).ReturnsDbSet(new List<Vendedor> { entity });

            var repository = new VendedorRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.RemoveAsync(entity));
            Assert.Null(exception);
        }
    }
}
