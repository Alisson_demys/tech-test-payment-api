﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Infrastructure.Context;
using pottencial_payment.Infrastructure.Repositories;
using pottencial_payment.Test.Configs;

namespace pottencial_payment.Test.Repository
{
    [Trait("Repository", "Repository Item")]
    public class ItemRepositoryTest
    {
        private readonly Mock<PottencialContext> _mockContext;
        private readonly Fixture _fixture;

        public ItemRepositoryTest()
        {
            _mockContext = new Mock<PottencialContext>(new DbContextOptionsBuilder<PottencialContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Lista Item")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Item>>();

            _mockContext.Setup(mock => mock.Set<Item>()).ReturnsDbSet(entities);

            var repository = new ItemRepository(_mockContext.Object);

            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Item Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Item>();

            _mockContext.Setup(mock => mock.Set<Item>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new ItemRepository(_mockContext.Object);

            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Item")]
        public async Task Post()
        {
            var entity = _fixture.Create<Item>();

            _mockContext.Setup(mock => mock.Set<Item>()).ReturnsDbSet(new List<Item> { new Item() });

            var repository = new ItemRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.AddAsync(entity));
            Assert.Null(exception);

        }

        [Fact(DisplayName = "Edita Item Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Item>();

            _mockContext.Setup(mock => mock.Set<Item>()).ReturnsDbSet(new List<Item> { new Item() });

            var repository = new ItemRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.EditAsync(entity));
            Assert.Null(exception);

        }

        [Fact(DisplayName = "Remove Item Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Item>();

            _mockContext.Setup(mock => mock.Set<Item>()).ReturnsDbSet(new List<Item> { entity });

            var repository = new ItemRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.RemoveAsync(entity));
            Assert.Null(exception);
        }
    }
}
