﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Infrastructure.Context;
using pottencial_payment.Infrastructure.Repositories;
using pottencial_payment.Test.Configs;

namespace pottencial_payment.Test.Repository
{
    [Trait("Repository", "Repository Venda")]
    public class VendaRepositoryTest
    {
        private readonly Mock<PottencialContext> _mockContext;
        private readonly Fixture _fixture;

        public VendaRepositoryTest()
        {
            _mockContext = new Mock<PottencialContext>(new DbContextOptionsBuilder<PottencialContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Lista Venda")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Venda>>();

            _mockContext.Setup(mock => mock.Set<Venda>()).ReturnsDbSet(entities);

            var repository = new VendaRepository(_mockContext.Object);

            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Venda Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Venda>();

            _mockContext.Setup(mock => mock.Set<Venda>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new VendaRepository(_mockContext.Object);

            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Venda")]
        public async Task Post()
        {
            var entity = _fixture.Create<Venda>();

            _mockContext.Setup(mock => mock.Set<Venda>()).ReturnsDbSet(new List<Venda> { new Venda() });

            var repository = new VendaRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.AddAsync(entity));
            Assert.Null(exception);

        }

        [Fact(DisplayName = "Edita Venda Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Venda>();

            _mockContext.Setup(mock => mock.Set<Venda>()).ReturnsDbSet(new List<Venda> { new Venda() });

            var repository = new VendaRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.EditAsync(entity));
            Assert.Null(exception);

        }

        [Fact(DisplayName = "Remove Venda Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Venda>();

            _mockContext.Setup(mock => mock.Set<Venda>()).ReturnsDbSet(new List<Venda> { entity });

            var repository = new VendaRepository(_mockContext.Object);

            var exception = await Record.ExceptionAsync(async () => await repository.RemoveAsync(entity));
            Assert.Null(exception);
        }
    }
}
