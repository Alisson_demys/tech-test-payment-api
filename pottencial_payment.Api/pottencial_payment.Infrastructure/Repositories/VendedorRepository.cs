﻿using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Repositories;
using pottencial_payment.Infrastructure.Context;

namespace pottencial_payment.Infrastructure.Repositories
{
    public class VendedorRepository : BaseRepository<Vendedor>, IVendedorRepository
    {
        public VendedorRepository(PottencialContext context) : base(context)
        {

        }
    }
}
