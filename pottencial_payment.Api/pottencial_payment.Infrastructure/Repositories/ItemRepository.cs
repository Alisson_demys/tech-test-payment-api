﻿using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Repositories;
using pottencial_payment.Infrastructure.Context;

namespace pottencial_payment.Infrastructure.Repositories
{
    public class ItemRepository : BaseRepository<Item>, IItemRepository
    {
        public ItemRepository(PottencialContext context) : base(context) { }

        //public async Task AdicionarVenda(Venda venda)
        //{
        //    await _context.Vendas.AddAsync(venda);
        //    await _context.SaveChangesAsync();
        //}
    }
}
