﻿using Microsoft.EntityFrameworkCore;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Infrastructure.Mappings;

namespace pottencial_payment.Infrastructure.Context
{
    public class PottencialContext : DbContext
    {
        public PottencialContext(DbContextOptions<PottencialContext> options) : base(options) { }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Item> Itens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendedor>(new VendedorMap().Configure);
            modelBuilder.Entity<Venda>(new VendaMap().Configure);
            modelBuilder.Entity<Item>(new ItemMap().Configure);
        }
    }
}
