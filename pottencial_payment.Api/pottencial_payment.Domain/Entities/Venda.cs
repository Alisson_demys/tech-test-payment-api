﻿using pottencial_payment.Domain.Enums;

namespace pottencial_payment.Domain.Entities
{
    public class Venda : BaseEntity
    {
        public Venda()
        {
            Status = StatusVendaEnum.AguardandoPagamento;
        }

        public StatusVendaEnum Status { get; set; }
        public DateTime DataVenda { get; set; }

        public virtual Vendedor Vendedor { get; set; }
        public int VendedorId { get; set; }

        public virtual ICollection<Item> Itens { get; set; }
    }
}
