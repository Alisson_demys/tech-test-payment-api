﻿namespace pottencial_payment.Domain.Entities
{
    public class Vendedor : BaseEntity
    {
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        //Reference
        public virtual ICollection<Venda> Vendas { get; set; }
    }
}
