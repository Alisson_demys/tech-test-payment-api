﻿namespace pottencial_payment.Domain.Entities
{
    public class Item : BaseEntity
    {
        public string Nome { get; set; }

        public virtual Venda Venda { get; set; }
        public int VendaId { get; set; }
    }
}
