﻿using pottencial_payment.Domain.Contracts.Item;
using System.ComponentModel.DataAnnotations;

namespace pottencial_payment.Domain.Contracts.Venda
{
    public class VendaRequest
    {
        [Required(ErrorMessage = "Data é obrigatório.")]
        public DateTime DataVenda { get; set; }

        [Required(ErrorMessage = "Id do Vendedor é obrigatório.")]
        public int VendedorId { get; set; }

        [Required(ErrorMessage = "Item é obrigatório.")]
        public ItemRequest[] Itens { get; set; }
    }
}
