﻿using System.ComponentModel.DataAnnotations;

namespace pottencial_payment.Domain.Contracts.Vendedor
{
    public class VendedorRequest
    {
        [Required(ErrorMessage = "CPF é obrigatório.")]
        [RegularExpression(@"[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}")]
        public string CPF { get; set; }

        [Required(ErrorMessage = "Nome é obrigatório.")]
        [StringLength(40, MinimumLength = 3)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Email é obrigatório.")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telefone é obrigatório.")]
        [RegularExpression(@"^\([1 - 9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0 - 9]{ 4}$")]
        public string Telefone { get; set; }
    }
}
