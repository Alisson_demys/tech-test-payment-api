﻿namespace pottencial_payment.Domain.Enums
{
    public enum StatusVendaEnum
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Cancelada,
        Entregue,
    }
}
