﻿using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Enums;
using pottencial_payment.Domain.Interfaces.Repositories;
using pottencial_payment.Domain.Interfaces.Services;
using pottencial_payment.Domain.Settings;

namespace pottencial_payment.Domain.Services
{
    public class VendaService : BaseService<Venda>, IVendaService
    {
        private readonly AppSetting _appSettings;
        private readonly IVendaRepository _repository;
        public VendaService(IVendaRepository repository, AppSetting appSettings) : base(repository)
        {
            _repository = repository;
            _appSettings = appSettings;
        }

        public async Task AtualizarStatus(int id, StatusVendaEnum status)
        {
            var entity = await ObterPorIdAsync(id);

            if (entity.Status == StatusVendaEnum.AguardandoPagamento && status == StatusVendaEnum.PagamentoAprovado ||
                entity.Status == StatusVendaEnum.AguardandoPagamento && status == StatusVendaEnum.Cancelada)
            {
                entity.Status = status;
            }

            else if (entity.Status == StatusVendaEnum.PagamentoAprovado && status == StatusVendaEnum.EnviadoParaTransportadora ||
                entity.Status == StatusVendaEnum.PagamentoAprovado && status == StatusVendaEnum.Cancelada)
            {
                entity.Status = status;
            }

            else if (entity.Status == StatusVendaEnum.EnviadoParaTransportadora && status == StatusVendaEnum.Entregue)
            {
                entity.Status = status;
            }
            else
            {
                throw new ArgumentException("Alteração de Status Inválida");
            }

            await _repository.EditAsync(entity);
        }
    }
}
