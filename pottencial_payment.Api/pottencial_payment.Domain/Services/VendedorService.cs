﻿using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Repositories;
using pottencial_payment.Domain.Interfaces.Services;
using pottencial_payment.Domain.Settings;

namespace pottencial_payment.Domain.Services
{
    public class VendedorService : BaseService<Vendedor>, IVendedorService
    {
        private readonly AppSetting _appSettings;
        private readonly IVendedorRepository _repository;
        public VendedorService(IVendedorRepository repository, AppSetting appSettings) : base(repository)
        {
            _repository = repository;
            _appSettings = appSettings;
        }
    }
}
