﻿using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Repositories;
using pottencial_payment.Domain.Interfaces.Services;

namespace pottencial_payment.Domain.Services
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {

        #region Constructor
        private readonly IBaseRepository<T> _repository;

        public BaseService(IBaseRepository<T> repository)
        {
            _repository = repository;
        }
        #endregion

        public async Task<IEnumerable<T>> ObterTodosAsync()
        {
            return await _repository.ListAsync();
        }

        public async Task<T> ObterPorIdAsync(int id)
        {
            var entity = await _repository.FindAsync(x => x.Id == id);
            if (entity == null)
                throw new ArgumentException($"Nenhum dado encontrado para o Id {id}");

            return entity;
        }

        public async Task AdicionarAsync(T entity)
        {
            await _repository.AddAsync(entity);
        }

        public async Task DeletarAsync(int id)
        {
            var entity = await _repository.FindAsync(id);
            if (entity == null)
                throw new ArgumentException($"Nenhum dado encontrado para o Id {id}");

            await _repository.EditAsync(entity);
        }

        public async Task AlterarAsync(T entity)
        {
            var find = await _repository.FindAsNoTrackingAsync(x => x.Id == entity.Id);
            if (find == null)
            {
                throw new ArgumentException($"Nenhum dado encontrado para o Id {entity.Id}");
            }
            await _repository.EditAsync(entity);
        }

    }
}
