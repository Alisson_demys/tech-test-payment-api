﻿using pottencial_payment.Domain.Entities;

namespace pottencial_payment.Domain.Interfaces.Services
{
    public interface IVendedorService : IBaseService<Vendedor>
    {
    }
}
