﻿namespace pottencial_payment.Domain.Interfaces.Services
{
    public interface IBaseService<T>
    {
        Task<IEnumerable<T>> ObterTodosAsync();
        Task<T> ObterPorIdAsync(int id);
        Task AdicionarAsync(T entity);
        Task DeletarAsync(int id);
        Task AlterarAsync(T entity);
    }
}
