﻿using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Enums;

namespace pottencial_payment.Domain.Interfaces.Services
{
    public interface IVendaService : IBaseService<Venda>
    {
        Task AtualizarStatus(int id, StatusVendaEnum status);
    }
}
