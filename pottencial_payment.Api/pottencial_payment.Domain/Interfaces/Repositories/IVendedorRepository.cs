﻿using pottencial_payment.Domain.Entities;

namespace pottencial_payment.Domain.Interfaces.Repositories
{
    public interface IVendedorRepository : IBaseRepository<Vendedor>
    {
    }
}
