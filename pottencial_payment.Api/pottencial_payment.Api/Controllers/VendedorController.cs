﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using pottencial_payment.Domain.Contracts.Vendedor;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Services;

namespace pottencial_payment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly IVendedorService _service;
        private readonly IMapper _mapper;

        public VendedorController(IVendedorService vendedorService, IMapper mapper)
        {
            _service = vendedorService;
            _mapper = mapper;
        }

        /// <summary>
        /// Através dessa rota você será capaz de cadastrar um Vendedor
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] VendedorRequest request)
        {
            var entity = _mapper.Map<Vendedor>(request);
            await _service.AdicionarAsync(entity);
            return Created(nameof(PostAsync), new { id = entity.Id });
        }

        /// <summary>
        /// Através dessa rota você será capaz de buscar uma listagem de Vendedores
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Sucesso, e retorna uma lista de elementos</response>
        [HttpGet]
        public async Task<ActionResult<List<VendedorResponse>>> GetAsync()
        {
            var entities = await _service.ObterTodosAsync();
            var response = _mapper.Map<IEnumerable<VendedorResponse>>(entities);
            return Ok(response);
        }

        /// <summary>
        /// Através dessa rota você será capaz de buscar um Vendedor pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpGet("{id}")]
        public async Task<ActionResult<List<VendedorResponse>>> GetByIdAsync([FromRoute] int id)
        {
            var entity = await _service.ObterPorIdAsync(id);
            var response = _mapper.Map<VendedorResponse>(entity);
            return Ok(response);
        }

        /// <summary>
        /// Através dessa rota você será capaz de alterar um Vendedor
        /// </summary>
        /// <param name="id"></param>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutAsync([FromRoute] int id, [FromBody] VendedorRequest request)
        {
            var entity = _mapper.Map<Vendedor>(request);
            entity.Id = id;
            await _service.AlterarAsync(entity);
            return NoContent();
        }

        /// <summary>
        /// Através dessa rota você será capaz de deletar um Vendedor
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _service.DeletarAsync(id);
            return NoContent();
        }
    }
}
