﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using pottencial_payment.Domain.Contracts.Venda;
using pottencial_payment.Domain.Entities;
using pottencial_payment.Domain.Interfaces.Services;

namespace pottencial_payment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService _service;
        private readonly IMapper _mapper;

        public VendaController(IVendaService vendaService, IMapper mapper)
        {
            _service = vendaService;
            _mapper = mapper;
        }

        /// <summary>
        /// Através dessa rota você será capaz de cadastrar um Venda
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] VendaRequest request)
        {
            var entity = _mapper.Map<Venda>(request);
            await _service.AdicionarAsync(entity);
            return Created(nameof(PostAsync), new { id = entity.Id });
        }

        /// <summary>
        /// Através dessa rota você será capaz de buscar uma listagem de Vendaes
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Sucesso, e retorna uma lista de elementos</response>
        [HttpGet]
        public async Task<ActionResult<List<VendaResponse>>> GetAsync()
        {
            var entities = await _service.ObterTodosAsync();
            var response = _mapper.Map<IEnumerable<VendaResponse>>(entities);
            return Ok(response);
        }

        /// <summary>
        /// Através dessa rota você será capaz de buscar um Venda pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpGet("{id}")]
        public async Task<ActionResult<List<VendaResponse>>> GetByIdAsync([FromRoute] int id)
        {
            var entity = await _service.ObterPorIdAsync(id);
            var response = _mapper.Map<VendaResponseItens>(entity);
            return Ok(response);
        }

        /// <summary>
        /// Através dessa rota você será capaz de alterar Status de um Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204">Sucesso, e retorna o elemento alterado via ID</response>
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromQuery] VendaStatusRequest request)
        {
            await _service.AtualizarStatus(id, request.Status);
            return Ok();
        }

        /// <summary>
        /// Através dessa rota você será capaz de alterar um Venda
        /// </summary>
        /// <param name="id"></param>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutAsync([FromRoute] int id, [FromBody] VendaRequest request)
        {
            var entity = _mapper.Map<Venda>(request);
            entity.Id = id;
            await _service.AlterarAsync(entity);
            return NoContent();
        }

        /// <summary>
        /// Através dessa rota você será capaz de deletar um Venda
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _service.DeletarAsync(id);
            return NoContent();
        }

    }
}
